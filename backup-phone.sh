#!/bin/bash

# First if Termux is not installed on phone go to Fdroid and install Termux.
# Make sure that NetGuard allows Termux to connect to the internet.
# Install openssh in Termux
# Run in Termux the following
# > pkg install openssh rsync

# Make sure sshd is run in Termux on the phone.

# This can be done either manually (open termux and enter 'sshd') or automatically
# after a one-time manual 'echo sshd >.profile' in termux and then
# starting termux via adb shell am as below from then on.

# If termux is already running, the following gives a warning
# indicating so; this is harmless: 'Warning: Activity not started,
# intent has been delivered to currently running top-most instance.'

# Note you must have adb installed on computer.
# > sudo apt install adb
# Go to About Phone > Build Number > Tap 7 times > Enter Phone Pin => You are are now a developer
# Connect phone to computer and allow File Transfer or Data permission
# On One Plus Phone go to System > Developer Options > Allow USB Debugging
# In computer terminal you should be able to see the phone as a connected device
# > adb devices -l
# To have a shell for the phone
# > adb shell

# phone Host is defined in ~/.ssh/config
# Crista host is oneplus
# nano ~/.ssh/config

# Copy public rsa key or file to phone device (manually) to (internal shared storage) storage/backup
# Go to Phone Settings > Apps > Termux > Allow Files and media permissions
# Go to Termux on phone and go to home directory
# > cd
# Check that permissions are allowed
# > ls /sdcard
# write the rsa pub id in backup to authorized keys on phone
# > cat /sdcard/backup/id_rsa.pub >>~/.ssh/authorized_keys
# Check that your public key was added
# cat >~/.ssh/authorized_keys
# On computer try
# > ssh -vvv oneplus
# If you use the following error message
# ssh_exchange_identification: read: Connection reset by peer
# FORCE STOP and RESTART TERMUX on phone otherwise ssh connection may be reset by peer.

adb shell am start -n com.termux/.HomeActivity

# sleep for 2 seconds to allow Termux to open and be ready
sleep 2s

# adb forward local remote
adb forward tcp:8022 tcp:8022

# Check that you can ssh with phone from computer
# > ssh -vvv oneplus

dest=~/backup/phone/oneplus
mkdir -p $dest

destext=~/backup/phone/oneplus/external
mkdir -p $destext

# rsync to/from both work out of the box for both Internal storage and
# External storage below. Here only 'from' (read, backup) is used.

# Internal storage
# /sdcard -> /storage/self/primary
# /storage/self/primary -> /storage/emulated/0
# make sure you have the correct hostname (oneplus for crista)
rsync -trP oneplus:/sdcard/Download $dest
rsync -trP oneplus:/sdcard/Music $dest
rsync -trP oneplus:/sdcard/Movies $dest
rsync -trP oneplus:/sdcard/Pictures $dest
#rsync -trP\
#      --exclude 'DCIM/' \
#      --exclude 'Pictures/' \
#      phone:/sdcard/ ~/backups/phone/int/

# name of external sd car
extsdcard=C0C9-DE11
# extsdcard=47BB-73FB

# External storage
# rsync -trP oneplus:/storage/${extsdcard}/{DCIM,Pictures} $destext
#rsync -trP\
#      --exclude 'DCIM/' \
#      --exclude 'Pictures/' \
#      phone:/storage/${extsdcard}/ ~/backups/phone/ext/
